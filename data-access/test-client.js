//
// File: test-client.js
// Auth: Martin Burolla
// Date: 9/20/2022
// Desc: A driver that tests the data acess library for our MySQL database.
//

const dataAccess = require('./data-access')

const main = async () => {
    // let rows = await dataAccess.selectAllCars()
    let rows = await dataAccess.selectCarForId(2)
    console.log(rows)

    process.exit()
}

main()
