const mysql = require('mysql2')

exports.createPromisePool = async () => {
    const pool = mysql.createPool({
        host: 'localhost', 
        user: 'xxx', 
        password: 'xxx', 
        database: 'cars'
    });
    return pool.promise();
}
