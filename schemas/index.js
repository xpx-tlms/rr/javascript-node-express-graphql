//
// File: index.js
// Auth: Martin Burolla
// Date: 9/20/2022
// Desc: Our GraphQL API.
//

const Queries = require('./queries')
const Mutations = require('./mutations')
const { GraphQLSchema } = require('graphql')

module.exports = new GraphQLSchema({
    query: Queries,
    mutation: Mutations
})
