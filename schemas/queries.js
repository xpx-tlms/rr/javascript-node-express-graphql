//
// File: queries.js
// Auth: Martin Burolla
// Date: 9/20/2022
// Desc: Contains all the read actions for the GraphQL API.
//

const userData = require('../MOCK_DATA.json')
const CarType = require('./TypeDefs/CarType')
const UserType = require('./TypeDefs/UserType')
const dataAccess = require('../data-access/data-access')

const { 
    GraphQLList,
    GraphQLObjectType, 
    GraphQLInt, 
    GraphQLString
} = require("graphql")

const Queries = new GraphQLObjectType({
    name: "Queries",
    fields: { 
        getMessage: { // <== Similar to a GET web API endpoint.
            type: GraphQLString,
            args: { },
            resolve(parent, args) {
                return "Hello World"
            }
        },
        getAllUsers: { // <== Similar to a GET web API endpoint.
            type: new GraphQLList(UserType),
            args: { },
            resolve(parent, args) {
                return userData
            }
        },
        getUserFirstName: { // <== Similar to a GET web API endpoint.
            type: GraphQLString,
            args: {
                id: { type: GraphQLInt }
            },
            resolve(parent, args) {
                return userData[args.id-1].first_name
            }
        },
        getUser: { // <== Similar to a GET web API endpoint.
            type: UserType,
            args: { 
                id: { type: GraphQLInt }
            },
            resolve(parent, args) {
                return userData[args.id-1]
            }
        },
        getCar: {
            type: CarType,
            args: { 
                id: { type: GraphQLInt }
            },
            async resolve(parent, args) {
                const car = await dataAccess.selectCarForId(args.id)
                return car
            }
        },

        //
        // TODO: Add more queries here...
        //

    }
})

module.exports = Queries
